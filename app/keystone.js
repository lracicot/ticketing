// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require('dotenv').config();

// Require keystone
var keystone = require('keystone');
var Twig = require('twig');
var fs = require('fs');
var path = require('path');

// create a write stream (in append mode)
var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' });

keystone.init({
	'name': 'ticketing',
	'brand': 'ticketing',

	'less': 'public',
	'static': 'public',
	'favicon': 'public/favicon.ico',
	'views': 'src/templates/views',
	'view engine': 'twig',

	'twig options': { method: 'fs' },
	'custom engine': Twig.render,

	'auto update': true,
	'session': true,
	'auth': true,
	'user model': 'User',

	'logger': ':method :url :status :response-time ms',
	'logger options': { stream: accessLogStream },
});
keystone.import('src/models');
keystone.set('locals', {
	_: require('lodash'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable,
});

console.log(process.env.MONGO_URI);
keystone.set('routes', require('./src/routes'));

keystone.set('nav', {
	users: ['users', 'groups', 'memberships'],
	events: ['events', 'ticket-types', 'discounts', 'attendees'],
	reservations: ['reservations', 'tickets', 'payments'],
});



keystone.start();
