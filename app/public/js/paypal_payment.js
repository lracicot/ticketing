(function ($) {
	$.fn.paypal_button = function (params) {
		var button_id = $(this).attr('id');
		var paypal_params = {
			env: params.paypal_env,
			locale: 'fr_CA',
			style: {
				size: 'medium',
				color: 'gold',
				shape: 'rect',
				label: 'pay',
			},
			client: {},
			commit: true,
			payment: function (data, actions) {
				return actions.payment.create({
					payment: {
						transactions: [{
							amount: { total: params.total_price, currency: 'CAD' },
						}],
					},
				});
			},
			onAuthorize: function (data, actions) {
				return actions.payment.execute().then(function (payment) {
					if (payment.state === 'approved' || payment.state === 'created') {

						$('#payment_validation_msg').modal({ kayboard: false });

						$.post(params.payment_validation_url, payment)
						.done(function (res) {
							if (res.state && res.state === 'approved') {
								$('#payment_sucess_msg').show();
								$('#payment_validation_msg').modal('hide');
								setTimeout(function () { window.location = '/profile/tickets'; }, 2000);
							} else {
								$('#payment_err_msg').show();
							}
						})
						.fail(function () {
							$('#payment_err_msg').show();
						})
						.always(function () {
							$('#payment_validation_msg').modal('hide');
						});
					} else {
						$('#payment_err_msg').show();
					}
				});
			},
			onError: function (err) {
				$('#payment_err_msg').show();
			}
		};

		paypal_params.client[params.paypal_env] = params.paypal_client_id;

		paypal.Button.render(paypal_params, '#' + button_id);
	};
})(jQuery);
