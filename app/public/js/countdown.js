(function ($) {

	function pad (n) { return ('0' + n).slice(-2); }

	$.fn.countdown = function () {
		$.each(this, function (k, self) {
			var timer = setInterval(function () {
				var time = $(self).html();

				if (time === '00:00' || time === '') {
					clearInterval(timer);
				} else {
					var date = Date.parse('January 1, 1970, 00:' + time + ' UTC');
					var newDate = new Date(date - 1000);
					var newTime = pad(newDate.getMinutes()) + ':' + pad(newDate.getSeconds());
					$(self).html(newTime);
				}
			}, 1000);
		});
	};

}(jQuery));
