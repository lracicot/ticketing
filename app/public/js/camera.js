(function ($) {

	var videoElement;
	var videoSelect;

	$.fn.camera = function (options) {
		videoElement = $(this).get(0);
		videoSelect = $(options.videoSelect).get(0);

		navigator.mediaDevices.enumerateDevices().then(gotDevices).then(getStream).catch(handleError);
		videoSelect.onchange = getStream;
	};

	function gotDevices (deviceInfos) {
		for (var i = 0; i !== deviceInfos.length; ++i) {
			var deviceInfo = deviceInfos[i];
			var option = document.createElement('option');
			option.value = deviceInfo.deviceId;
			if (deviceInfo.kind === 'videoinput') {
				option.text = deviceInfo.label || 'camera ' + (videoSelect.length + 1);
				videoSelect.appendChild(option);
			}
		}
	}

	function getStream () {
		if (window.stream) {
			window.stream.getTracks().forEach(function (track) {
				track.stop();
			});
		}

		var constraints = {
			video: {
				optional: [{
					sourceId: videoSelect.value,
				}],
			},
		};

		navigator.mediaDevices.getUserMedia(constraints).then(gotStream).catch(handleError);
	}

	function gotStream (stream) {
		window.stream = stream;
		videoElement.srcObject = stream;
	}

	function handleError (error) {
		console.log('Error: ', error);
	}

}(jQuery));
