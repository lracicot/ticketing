(function ($) {
	$.fn.accumulate = function (accumulator) {
		var sum = 0;
		$(this).each(function () {
			sum += parseInt(accumulator(this));
		});
		return sum;
	};
})(jQuery);
