var keystone = require('keystone');
var Discount = keystone.list('Discount');
var Attendee = keystone.list('Attendee');

exports = module.exports = async function (req, res) {

	try {
		const attendee = await Attendee.model.findOne({ _id: req.params.attendee_id }).populate('reservation').exec();
		const discount = await Discount.model.findOne({
			code: req.body.code,
			event: attendee.reservation.event,
			used: false,
		}).exec();

		discount.used = true;
		discount.attendee = attendee._id;
		discount.save();
	} catch (err) {
		console.log(err);
		req.flash('error', 'Code rabais introuvable');
	}

	res.redirect(req.body.redirect);
	res.send();
};
