var keystone = require('keystone');
var paypal = require('paypal-rest-sdk');
var Payment = keystone.list('Payment');
var Reservation = keystone.list('Reservation');
var Attendee = keystone.list('Attendee');
var Ticket = keystone.list('Ticket');
var Event = keystone.list('Event');

exports = module.exports = async (req, res) => {

	paypal.configure({
		mode: process.env.PAYPAL_ENV,
		client_id: process.env.PAYPAL_CLIENT_ID,
		client_secret: process.env.PAYPAL_SECRET,
	});

	// TODO: Add optional param for attendee id. If its not there, the total amount should be remaining price
	paypal.payment.get(req.body.id, async (error, payment) => {
		try {
			if (error) {
				console.log(error);
				throw error;
			} else {
				if (payment.state === 'approved') {

					const event = await Event.model.findById(req.params.id);
					const reservationData = await req.user.getEventReservation(event._id);
					const reservation = await Reservation.model.findById(reservationData[0]._id);

					// Save payment to database
					Payment.model.create({
						user: req.user._id,
						reservation: reservation._id,
						data: JSON.stringify(payment),
					});

					const attendees = await reservation.getAttendees();

					if (req.params.attendee_id) {
						// Mark the attendee as paid
						// TODO: We need to make sure that amout === ticket price
						const attendee = await Attendee.model.findOne({ _id: req.params.attendee_id }).populate('ticket_type').exec();

						if (parseFloat(payment.transactions[0].amount.total) === parseFloat(await attendee.getPrice())) {
							attendee.paid = true;
							await attendee.save();
						} else {
							throw String('Attendee payment does not have the required amount.');
						}
					} else {
						if (parseFloat(payment.transactions[0].amount.total) === parseFloat(await reservation.getRemainingPrice())) {
							// Mark all attendees as paid
							for (const a of attendees) {
								a.paid = true;
								await a.save();
							}
						} else {
							throw String('Reservation payment does not have the required amount.');
						}
					}

					// If all attendees are paid, generate the tickets
					// TODO: What happend if, at this point, there is no ticket left ?
					if (await reservation.isPaid() && (await reservation.getTickets()).length === 0) {
						for (const a of attendees) {
							Ticket.model.create({
								event: event._id,
								reservation: reservation._id,
								user: a.user,
								email: a.email,
								name: a.name,
								ticket_type: a.ticket_type._id,
							});
						}
					}
				}
			}

			res.setHeader('Content-Type', 'application/json');
			res.send(JSON.stringify({ state: payment.state }));

		} catch (err) {
			console.log(err);
		}
	});
};
