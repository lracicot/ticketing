var keystone = require('keystone');
var Event = keystone.list('Event');
var Reservation = keystone.list('Reservation');
var Attendee = keystone.list('Attendee');
var moment = require('moment');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.section = 'events';

	view.on('get', async next => {
		try {
			locals.reservation = await Reservation.model.findById(req.params.reservation_id);
			locals.attendee = await Attendee.model.findOne({ _id: req.params.attendee_id }).populate('ticket_type').exec();

			if (!locals.reservation) {
				return res.redirect('/');
			}

			if (await locals.attendee.paid) {
				return res.redirect('/profile/tickets');
			}

			if (!locals.reservation.isStillValid) {
				req.flash('error', 'Sold out.');
				return res.redirect('/event/' + locals.reservation.event);
			}

			if (moment().add(5, 'm').isAfter(locals.reservation.unpaidTimeout)) {
				locals.reservation.setTimeoutTo(5, 'm');
				locals.reservation.save();
			}

			locals.remainingTime = locals.reservation.remainingTime.format('mm:ss');

			locals.paypal = {
				env: process.env.PAYPAL_ENV,
				client_id: process.env.PAYPAL_CLIENT_ID,
			};

			locals.event = await Event.model.findById(locals.reservation.event);

			locals.total_price = await locals.attendee.getPrice();
			locals.discount = await locals.attendee.getDiscount();
			next();
		} catch (err) {
			console.log(err);
			next(err);
		}
	});

	view.render('payment/attendee');
};
