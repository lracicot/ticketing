var keystone = require('keystone');
var Event = keystone.list('Event');
var Reservation = keystone.list('Reservation');
var moment = require('moment');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.section = 'events';

	view.on('get', async next => {
		try {
			locals.reservation = await Reservation.model.findById(req.params.reservation_id);

			if (!locals.reservation) {
				return res.redirect('/');
			}

			if (await locals.reservation.isPaid()) {
				return res.redirect('/profile/tickets');
			}

			if (!locals.reservation.isStillValid) {
				req.flash('error', 'Sold out.');
				return res.redirect('/event/' + locals.reservation.event);
			}

			if (moment().add(5, 'm').isAfter(locals.reservation.unpaidTimeout)) {
				locals.reservation.setTimeoutTo(5, 'm');
				locals.reservation.save();
			}

			locals.remainingTime = locals.reservation.remainingTime.format('mm:ss');

			locals.paypal = {
				env: process.env.PAYPAL_ENV,
				client_id: process.env.PAYPAL_CLIENT_ID,
			};

			locals.attendees = await locals.reservation.getAttendees();

			for (let attendee of locals.attendees) {
				attendee.price = await attendee.getPrice();
				attendee.discount = await attendee.getDiscount();
			}

			// TODO: Should be the remaining price, in case some attendees already paid.
			// TODO: Add a nice message if remaining price is 0
			locals.total_price = await locals.reservation.getRemainingPrice();
			locals.event = await Event.model.findById(locals.reservation.event);
			next();
		} catch (err) {
			next(err);
		}
	});

	view.render('payment/reservation');
};
