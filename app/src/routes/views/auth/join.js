var keystone = require('keystone');
var asynchron = require('async');
var Ticket = keystone.list('Ticket');
var Membership = keystone.list('Membership');

exports = module.exports = function (req, res) {

	if (req.user) {
		return res.redirect('/profile/tickets');
	}
	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.section = 'createaccount';
	locals.form = req.body;

	if (req.query.email) {
		locals.form.email = req.query.email;
	}


	view.on('post', function (next) {
		asynchron.series([
			function (cb) {
				if (!req.body.firstname || !req.body.lastname || !req.body.email || !req.body.password) {
					req.flash('error', 'Please enter a username, your name, email and password.');
					return cb(true);
				}

				return cb();
			},
			function (cb) {
				keystone.list('User').model.findOne({ email: req.body.email }, function (err, user) {
					if (err || user) {
						req.flash('error', 'User already exists with that email address.');
						return cb(true);
					}

					return cb();
				});
			},
			// function (cb) {
			// 	if (
			// 		req.body.email.substr(req.body.email.length - 11) === '@polymtl.ca'
			// 		|| req.body.email.substr(req.body.email.length - 9) === 'etsmtl.ca'
			// 	) {
			// 		return cb();
			// 	}
			//
			// 	req.flash('error', 'L\'adresse doit finir par @polymtl.ca ou par etsmtl.ca');
			// 	return cb(true);
			// },
			function (cb) {
				var userData = {
					name: {
						first: req.body.firstname,
						last: req.body.lastname,
					},
					company: req.body.company,
					email: req.body.email,
					password: req.body.password,
				};

				var User = keystone.list('User').model;
				var newUser = new User(userData);

				newUser.save(async function (err) {
					try {
						// Check if new user have tickets
						const tickets = await Ticket.model.find({ email: req.body.email }).exec();

						for (const ticket of tickets) {
							ticket.user = newUser._id;
							ticket.name = newUser.full_name;
							ticket.save();
						}

						// check if new user have memberships
						const memberships = await Membership.model.find({ email: req.body.email }).exec();

						for (const membership of memberships) {
							membership.user = newUser._id;
							membership.save();
						}
					} catch (err) {
						console.log(err);
					}

					return cb(err);
				});
			},

		], function (err) {

			if (err) return next();

			var onSuccess = function () {
				res.redirect('/profile/tickets');
			};

			var onFail = function (e) {
				req.flash('error', 'There was a problem signing you up, please try again.');
				return next();
			};

			keystone.session.signin({ email: req.body.email, password: req.body.password }, req, res, onSuccess, onFail);

		});

	});

	view.render('auth/join');
};
