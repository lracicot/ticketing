var keystone = require('keystone');

exports = module.exports = function (req, res) {
	if (req.user) {
		return res.redirect('/');
	}

	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.section = 'signin';
	locals.redirect = '/';

	if (req.query.redirect) {
		locals.redirect = new Buffer(req.query.redirect, 'base64').toString('utf8');
	}

	if (req.body.redirect) {
		locals.redirect = req.body.redirect;
	}

	view.on('post', function (next) {
		if (!req.body.email || !req.body.password) {
			req.flash('error', 'Please enter your email and password.');
			return next();
		}

		var onSuccess = function () {
			return res.redirect(locals.redirect);
		};

		var onFail = function () {
			req.flash('error', 'Input credentials were incorrect, please try again.');
			return next();
		};

		keystone.session.signin({ email: req.body.email, password: req.body.password }, req, res, onSuccess, onFail);
	});

	view.render('auth/signin');
};
