var keystone = require('keystone');
var Ticket = keystone.list('Ticket');

exports = module.exports = (req, res) => {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.section = '';

	view.on('post', async next => {
		try {
			const ticket = await Ticket.model.findOne({ _id: req.body.code, used: false });

			if (ticket) {
				ticket.used = true;
				ticket.save();
				res.send({ status: 'valid', ticket: ticket });
			} else {
				res.send({ status: 'invalid' });
			}
		} catch (err) {
			res.send({ status: 'invalid' });
		}
	});

	view.render('organise/scanner');
};
