var keystone = require('keystone');
var User = keystone.list('User');
var Event = keystone.list('Event');
var Ticket = keystone.list('Ticket');

exports = module.exports = (req, res) => {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.section = '';

	view.on('get', async next => {
		try {
			locals.events = await Event.model.find().exec();

			for (const event of locals.events) {
				event.ticket_types = await event.getTicketTypes();
			}

			locals.users = await User.model.find().exec();
			next();

		} catch (err) {
			console.log(err);
			next();
		}
	});

	view.on('post', async next => {
		try {
			const user = await User.model.findById(req.body.user);

			Ticket.model.create({
				event: req.body.event,
				user: req.body.user,
				email: user.email,
				name: user.full_name,
				ticket_type: req.body.ticket_type,
			});
			next();

		} catch (err) {
			console.log(err);
			next();
		}
	});

	view.render('organise/tickets');
};
