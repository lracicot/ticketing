var _ = require('lodash');
var keystone = require('keystone');
var Ticket = keystone.list('Ticket');
var Reservation = keystone.list('Reservation');
var Attendee = keystone.list('Attendee');
var mongoose = require('mongoose');

exports = module.exports = (req, res) => {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.section = '';

	view.on('get', async next => {
		/* Attendee.model.create({
		    name: 'Alex-Sandra Mailhot',
		    reservation: '59c1da8ecddf9629bc2a1096',
		    paid: true,
		    ticket_type: '59b00dc132d760662dabce39',
		});
		Attendee.model.create({
		    name: 'Pier-Olivier Veilleux',
		    reservation: '59c1da8ecddf9629bc2a1096',
		    paid: true,
		    ticket_type: '59b00db332d760662dabce38',
		});
		Attendee.model.create({
		    name: 'Florence Di Lallo Gohier',
		    reservation: '59c1da8ecddf9629bc2a1096',
		    paid: true,
		    ticket_type: '59b00db332d760662dabce38',
		});
		Attendee.model.create({
		    name: 'Alex-Sandra Mailhot',
		    reservation: '59c1da8ecddf9629bc2a1096',
		    paid: true,
		    ticket_type: '59b00db332d760662dabce38',
		});
		Attendee.model.create({
		    name: 'Anne-Louise Normand',
		    reservation: '59c1da8ecddf9629bc2a1096',
		    paid: true,
		    ticket_type: '59b00db332d760662dabce38',
		});
		Attendee.model.create({
		    name: 'Chloé Mayer',
		    reservation: '59c1da8ecddf9629bc2a1096',
		    paid: true,
		    ticket_type: '59b00db332d760662dabce38',
		});
		Attendee.model.create({
		    name: 'Natacha Warin',
		    reservation: '59c1da8ecddf9629bc2a1096',
		    paid: true,
		    ticket_type: '59b00db332d760662dabce38',
		});
		Attendee.model.create({
		    name: 'Justine Dufresne',
		    reservation: '59c1da8ecddf9629bc2a1096',
		    paid: true,
		    ticket_type: '59b00db332d760662dabce38',
		});

		Attendee.model.create({
		    name: 'Justine Bastien',
		    reservation: '59c1dcf84cf60d2a1876b2c2',
		    paid: true,
		    ticket_type: '5959b00dc132d760662dabce39',
		});
		Attendee.model.create({
		    name: 'Camille Genèt',
		    reservation: '59c1dcf84cf60d2a1876b2c2',
		    paid: true,
		    ticket_type: '59b00dc132d760662dabce39',
		});
		Attendee.model.create({
		    name: 'Grégoire Paré',
		    reservation: '59c1dcf84cf60d2a1876b2c2',
		    paid: true,
		    ticket_type: '59b00db332d760662dabce38',
		});
		Attendee.model.create({
		    name: 'Olivier Pageau',
		    reservation: '59c1dcf84cf60d2a1876b2c2',
		    paid: true,
		    ticket_type: '59b00db332d760662dabce38',
		});
		Attendee.model.create({
		    name: 'Laurent Pellerin-Boudriau',
		    reservation: '59c1dcf84cf60d2a1876b2c2',
		    paid: true,
		    ticket_type: '59b00db332d760662dabce38',
		});
		Attendee.model.create({
		    name: 'Camille Lefebvre',
		    reservation: '59c1dcf84cf60d2a1876b2c2',
		    paid: true,
		    ticket_type: '59b00db332d760662dabce38',
		});
		Attendee.model.create({
		    name: 'Thierry Major-Cyr',
		    reservation: '59c1dcf84cf60d2a1876b2c2',
		    paid: true,
		    ticket_type: '59b00db332d760662dabce38',
		});
		Attendee.model.create({
		    name: 'Antoine Brisset',
		    reservation: '59c1dcf84cf60d2a1876b2c2',
		    paid: true,
		    ticket_type: '59b00db332d760662dabce38',
		});

		Attendee.model.create({
		    name: 'Amber Tunney',
		    reservation: '59c1dd154cf60d2a1876b2c3',
		    paid: true,
		    ticket_type: '59b00dc132d760662dabce39',
		});
		Attendee.model.create({
		    name: 'Amélie Laflamme',
		    reservation: '59c1dd154cf60d2a1876b2c3',
		    paid: true,
		    ticket_type: '59b00dc132d760662dabce39',
		});
		Attendee.model.create({
		    name: 'Marc-Olivier Bien-aimé',
		    reservation: '59c1dd154cf60d2a1876b2c3',
		    paid: true,
		    ticket_type: '59b00db332d760662dabce38',
		});
		Attendee.model.create({
		    name: 'Maxime Savaria',
		    reservation: '59c1dd154cf60d2a1876b2c3',
		    paid: true,
		    ticket_type: '59b00db332d760662dabce38',
		});
		Attendee.model.create({
		    name: 'Tommy Lavoie',
		    reservation: '59c1dd154cf60d2a1876b2c3',
		    paid: true,
		    ticket_type: '59b00db332d760662dabce38',
		});
		Attendee.model.create({
		    name: 'Steve Orellana',
		    reservation: '59c1dd154cf60d2a1876b2c3',
		    paid: true,
		    ticket_type: '59b00db332d760662dabce38',
		});
		Attendee.model.create({
		    name: 'Amélie Laflamme',
		    reservation: '59c1dd154cf60d2a1876b2c3',
		    paid: true,
		    ticket_type: '59b00db332d760662dabce38',
		});*/


		/* for (const reservation of await Reservation.model.find().exec()) {
			console.log(reservation._id);
			for (const attendee of await reservation.getAttendees()) {

				let name = attendee.name;

				if ((await keystone.list('Discount').model.find({ attendee: attendee._id }).exec()).length > 0) {
					name += ' , privilege';
				}

				console.log(name);
			}

		}*/

		/* let reservations = {};
		let attendees = {};
		let buggy = {};
		let toDelete = [];
		let toDeleteUniq = [];

		// Group attendees by email
		for (const attendee of await Attendee.model.find().lean().exec()) {

			attendee.reservation = String(attendee.reservation);

			if (!attendees[attendee.email]) {
				attendees[attendee.email] = [];
			}

			if (!reservations[attendee.reservation]) {
				reservations[attendee.reservation] = [];
			}

			if (attendee.email === 'false') {
				toDelete.push(attendee.reservation);
			}

			attendees[attendee.email].push(attendee.reservation);
			reservations[attendee.reservation].push(attendee.email);
		}

		toDeleteUniq = _.uniq(toDelete);

		// Place emails with more than one reservation in the buggy list
		for (const email in attendees) {
			if (!attendees.hasOwnProperty(email)) continue;

			for (const k in attendees[email]) {
				if (toDelete.indexOf(attendees[email][k]) !== -1) {
					// Actually delete this reservation and all related attendees
					await Attendee.model.remove({ reservation: attendees[email][k] });
					await Reservation.model.remove({ _id: attendees[email][k] });
					delete attendees[email][k];
				}
			}

			if (attendees[email].length > 1) {
				buggy[email] = attendees[email];
				delete attendees[email];
			}
		}

		for (const k in buggy) {
			buggy[k].sort();
			// Actually delete this reservation and all related attendees
			await Attendee.model.remove({ reservation: buggy[k][0] });
			await Reservation.model.remove({ _id: buggy[k][0] });
		}

		console.log(toDeleteUniq);
		console.log(buggy);*/

		for (const reservation of await Reservation.model.find().exec()) {
			if ((await reservation.getTickets()).length === 0) {

				const attendees = await reservation.getAttendees();
				for (const a of attendees) {
					Ticket.model.create({
						event: '59b00d7732d760662dabce37',
						reservation: reservation._id,
						user: a.user,
						email: a.email,
						name: a.name,
						ticket_type: a.ticket_type._id,
					});
				}
			}
		}
		next();
	});

	view.render('organise/dashboard');
};
