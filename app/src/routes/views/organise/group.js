var keystone = require('keystone');
var User = keystone.list('User');
var Group = keystone.list('Group');
var Membership = keystone.list('Membership');

exports = module.exports = (req, res) => {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.section = '';

	view.on('get', async next => {
		try {
			locals.groups = await Group.model.find({ owner: req.user._id }).exec();
			next();

		} catch (err) {
			console.log(err);
			next();
		}
	});

	view.on('post', async next => {
		try {
			const group = await Group.model.findById(req.params.group_id);
			const members = await group.getMembers();

			for (const email of req.body.emails.split(/\r?\n/)) {
				// If user or email not already a member
				if (members.find(member => {
					return member.email === email || member.user && member.user.email === email;
				})) continue;

				let newUser = {
					group: group._id,
					email: email,
				};

				// If user exists
				const user = await User.model.findOne({ email: email }).exec();
				if (user) {
					newUser.user = user._id;
				}

				Membership.model.create(newUser);
			}

			res.redirect('/organise/groups');

		} catch (err) {
			console.log(err);
			next();
		}
	});

	view.render('organise/groups');
};
