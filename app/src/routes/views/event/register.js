var keystone = require('keystone');
var _ = require('lodash');
var Event = keystone.list('Event');
var Reservation = keystone.list('Reservation');
var Attendee = keystone.list('Attendee');
var User = keystone.list('User');
var Ticket = keystone.list('Ticket');
const send_mail = require('./../../../services/email');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.section = 'events';

	view.on('get', async next => {
		try {
			locals.event = await Event.model.findById(req.params.id);
			locals.reservation = await Reservation.model.findById(req.params.reservation_id);

			locals.time_remaining = locals.reservation.remainingTime.format('mm:ss');
			locals.total_price = await locals.reservation.getTotalPrice();

			locals.attendees = await locals.reservation.getAttendees();

			locals.myself = _.remove(locals.attendees, attendee => {
				return attendee.user && attendee.user !== req.user._id;
			})[0];

			next();
		} catch (err) {
			console.log(err);
			return next(err);
		}
	});

	view.on('post', async next => {

		try {
			const reservation = await Reservation.model.findById(req.params.reservation_id);
			const event = await Event.model.findById(reservation.event);

			if (req.body.attendees) {
				for (const attendee_id of req.body.attendees) {
					if (req.user.email === req.body.guests_emails[attendee_id]) continue;

					let skip = false;

					// Ugly hack to prevent people from having two reservations
					for (const existingAttendee of await Attendee.model.find({ email: req.body.guests_emails[attendee_id] }).populate('reservation').exec()) {
						if (existingAttendee.reservation && existingAttendee.reservation.event === event._id) {
							skip = true;
						}
					}

					if (skip === true) continue;

					const attendee = await Attendee.model.findById(attendee_id);

					attendee.email = req.body.guests_emails[attendee_id];
					attendee.name = req.body.guests_names[attendee_id];
					const user = await User.model.findOne({ email: attendee.email }).exec();

					if (user) {
						attendee.user = user;
						attendee.name = user.full_name;
					} else {
						// Send email to join.
						send_mail(
							'src/templates/emails/join.twig', {
								event: event,
								full_name: attendee.name,
								join_link: 'https://billetterie.aep.polymtl.ca/join?email=' + attendee.email,
							},
							attendee.email,
							'Joindre la billetterie de l\'AEP'
						);
					}

					if (req.body.individual_payment === '1') {
						send_mail(
							'src/templates/emails/invitation.twig', {
								event: event,
								full_name: attendee.name,
								payment_link: 'https://billetterie.aep.polymtl.ca/payment/' + reservation._id + '/' + attendee._id,
							},
							attendee.email,
							'Invitation à un événement'
						);
					}

					attendee.save();
				}
			}

			reservation.individual_payment = req.body.individual_payment === '1';
			// Incrase remaining time to 20 minutes
			reservation.setTimeoutTo(20, 'm');
			reservation.save();

			if (0 == parseFloat(await reservation.getRemainingPrice())) {
				if ((await reservation.getTickets()).length === 0) {
					for (const a of (await reservation.getAttendees())) {
						a.paid = true;
						await a.save();
						Ticket.model.create({
							event: event._id,
							reservation: reservation._id,
							user: a.user,
							email: a.email,
							name: a.name,
							ticket_type: a.ticket_type._id,
						});
					}
				}

				res.redirect('/profile/tickets');
			} else if (req.body.individual_payment === '0') {
				res.redirect('/payment/' + reservation.id);
			} else {
				const myself = await reservation.getAttendeeByUser(req.user._id);
				res.redirect('/payment/' + reservation.id + '/' + myself._id);
			}
		} catch (err) {
			console.log(err);
			return next(err);
		}
	});

	view.render('event/register');
};
