var keystone = require('keystone');
var Event = keystone.list('Event');
var Reservation = keystone.list('Reservation');
var Attendee = keystone.list('Attendee');

var moment = require('moment');
var _ = require('lodash');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.section = 'events';

	view.on('post', async next => {

		try {
			const event = await Event.model.findOne({ _id: req.params.id }).populate('ticket_types').exec();

			// If registrations are not opened yet opened
			if (!event.areRegistrationsOpen()) {
				req.flash('error', 'Registration are closed');
				return res.redirect('/event/' + req.params.id);
			}

			// If user already have a reservation for this event
			if ((await req.user.getEventReservation(event._id)).length > 0) {
				req.flash('error', 'You already have a reservation for this event. <a href="/profile/tickets">View your tickets</a>');
				return res.redirect('/event/' + req.params.id);
			}

			// If event is restricted to a group
			if (event.group && !(await req.user.isInGroup(event.group))) {
				req.flash('error', 'You are not in an authorized group.');
				return res.redirect('/event/' + req.params.id);
			}

			let number_of_tickets = 1;

			if (req.body.number_of_tickets) {
				number_of_tickets += _.reduce(req.body.number_of_tickets, (prev, curr) => prev + parseInt(curr), 0);
			}

			// If invalid number of tickets
			const maxError = number_of_tickets > event.max_buy_tickets;
			const minError = number_of_tickets < event.min_buy_tickets;
			
			if (maxError) {
				req.flash('error', 'Invalid number of tickets: ' + number_of_tickets + '. Maximum is ' + event.max_buy_tickets);
				return res.redirect('/event/' + req.params.id);
			}
			
			if (minError) {
				req.flash('error', 'Invalid number of tickets: ' + number_of_tickets + '. Minimum is ' + event.min_buy_tickets);
				return res.redirect('/event/' + req.params.id);
			}

			for (const type of await event.getTicketTypes()) {
				if (type.max_buy > 0
					&& req.body.number_of_tickets
					&& req.body.number_of_tickets[type._id]
					&& parseInt(req.body.number_of_tickets[type._id]) + (req.body.my_ticket_type === type._id ? 1 : 0) > type.max_buy
				) {
					req.flash('error', 'Invalid number of tickets for ' + type.description);
					return res.redirect('/event/' + req.params.id);
				}
			}

			if (req.body.number_of_tickets > await event.getAvailableTickets()) {
				req.flash('error', 'Sold out!');
				return res.redirect('/event/' + req.params.id);
			}

			// Create the reservation
			var newReservation = new Reservation.model({
				unpaidTimeout: moment().add(10, 'm').toDate(),
				number_of_tickets: number_of_tickets,
				event: event._id,
			});

			await newReservation.save();

			// Add current user as attendee
			Attendee.model.create({
				paid: false,
				user: req.user._id,
				email: req.user.email,
				name: req.user.full_name,
				reservation: newReservation._id,
				ticket_type: req.body.my_ticket_type,
			});

			// Create the other attendees
			for (const ticket_type in req.body.number_of_tickets) {
				if (req.body.number_of_tickets[ticket_type]) {
					for (let i = 0; i < req.body.number_of_tickets[ticket_type]; i++) {
						Attendee.model.create({
							paid: false,
							reservation: newReservation._id,
							ticket_type: ticket_type,
						});
					}
				}
			}

			return res.redirect('/event/' + req.params.id + '/register/' + newReservation._id);
		} catch (err) {
			console.log(err);
			return next(err);
		}
	});

	// For some reasons, we need this or the browser never gets the redirect response
	view.render('index');
};
