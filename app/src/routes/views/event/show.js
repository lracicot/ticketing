var keystone = require('keystone');
var Event = keystone.list('Event');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.section = 'events';


	view.on('init', async next => {
		try {
			const eventFound = await Event.model.findById(req.params.id);

			if (!eventFound) {
				req.flash('error', 'Sorry, that event does not exists.');
				return res.redirect('/events');
			}

			locals.event = eventFound;
			locals.ticketTypes = await eventFound.getTicketTypes();
			locals.ticketsAvailable = await eventFound.getAvailableTickets();
			locals.registratonsOpened = eventFound.areRegistrationsOpen();
			next();

		} catch (err) {
			console.log(err);
			return next(err);
		}
	});

	view.render('event/show');
};
