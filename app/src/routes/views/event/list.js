var keystone = require('keystone');
var Event = keystone.list('Event');

exports = module.exports = (req, res) => {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.section = 'events';

	view.on('get', async next => {
		try {
			locals.events = await Event.model.find().sort('registration_start').exec();

			for (const event of locals.events) {
				event.ticket_types = await event.getTicketTypes();
			}
		} catch (err) {
			return next(err);
		}
		next();
	});

	view.render('event/list');
};
