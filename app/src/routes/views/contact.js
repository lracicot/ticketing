var keystone = require('keystone');
var Email = require('keystone-email');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'contact';
	locals.formData = req.body || {};
	locals.validationErrors = {};
	locals.enquirySubmitted = false;

	view.on('post', { action: 'contact' }, function (next) {
		new Email('src/templates/emails/contact.twig', {
			transport: 'mailgun',
		}).send({
			name: req.body.name,
			email: req.body.email,
			phone: req.body.phone,
			message: req.body.message,
		}, {
			to: process.env.ADMIN_EMAIL,
			from: {
				name: req.body.name,
				email: req.body.email,
			},
			subject: 'Contact form de la billetterie AEP',
		}, function (err, result) {
			if (err) {
				console.error('🤕 Mailgun test failed with error:\n', err);
				req.flash('error', 'Une erreure est survenue. Veuillez contacter le STEP.');
				next();
			} else {
				console.log('📬 Successfully sent Mailgun test with result:\n', result);
				req.flash('success', 'Nous vous contacterons prochainement.');
				next();
			}
		});
	});

	view.render('contact');
};
