var keystone = require('keystone');
var CustomField = keystone.list('CustomField');
var Membership = keystone.list('Membership');

exports = module.exports = (req, res) => {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.section = 'event';

	view.on('get', async next => {
		try {
			locals.memberships = await req.user.getMemberships();
			locals.customfields = {};

			for (const membership of locals.memberships) {
				locals.customfields[membership._id] = await CustomField.model.find({ group: membership.group._id });
			}
		} catch (err) {
			console.log(err);
			return next(err);
		}
		next();
	});

	view.on('post', async next => {
		try {
			const membership = await Membership.model.findById(req.params.membership_id);

			for (const field in req.body) {
				if (!req.body.hasOwnProperty(field)) continue;
				console.log('Will set ', field, ' to value "', req.body[field], '"');
				await membership.set(field, req.body[field].toString());
			}

			await membership.save();

		} catch (err) {
			console.log(err);
			return next(err);
		}
		res.redirect('/memberships');
	});

	view.render('profile/memberships');
};
