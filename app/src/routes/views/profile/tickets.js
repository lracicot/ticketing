var keystone = require('keystone');
var Reservation = keystone.list('Reservation');

exports = module.exports = (req, res) => {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.section = 'event';

	view.on('get', async next => {

		try {
			locals.reservations = await req.user.getReservations();

			for (const reservation of locals.reservations) {

				// Get Mongoose Object (mObj) for virtual properties and methods
				const mObj = await Reservation.model.findById(reservation._id);
				reservation.payable = await mObj.isStillValid();
				reservation.expired = mObj.isExpired;
				reservation.time_remaining = mObj.remainingTime.format('mm:ss');
				reservation.paid_for_current_user = await mObj.isPaidFor(req.user._id);

				// Remove paid reservations
				for (const key in locals.reservations) {
					if (!locals.reservations.hasOwnProperty(key)) continue;

					const r = locals.reservations[key];
					if (String(r._id) === String(mObj._id) && await mObj.isPaid()) {
						locals.reservations.splice(key, 1);
					}
				}
			}

			locals.tickets = await req.user.getTickets().populate('event').exec();

		} catch (err) {
			console.log(err);
			return next(err);
		}
		next();
	});

	view.on('delete', async next => {
		try {
			await Reservation.model.findById(req.params.reservation_id).remove();
		} catch (err) {
			return next(err);
		}
		res.end();
	});

	view.render('profile/ticket_list');
};
