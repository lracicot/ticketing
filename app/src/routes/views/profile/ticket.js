var keystone = require('keystone');
var Ticket = keystone.list('Ticket');
var Event = keystone.list('Event');
var QRCode = require('qrcode');

exports = module.exports = (req, res) => {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.section = 'event';

	view.on('get', async next => {

		try {
			const ticket = await Ticket.model.findById(req.params.ticket_id);

			if (!ticket) {
				return res.redirect('/profile/tickets');
			}

			locals.tickets = await Ticket.model.find({ reservation: ticket.reservation }).populate('ticket_type').lean().exec();

			locals.event = await Event.model.findById(ticket.event);

			const check = function (k) {
				if (k + 1 === locals.tickets.length) next();
			};

			for (let k = 0; k < locals.tickets.length; k++) {
				QRCode.toDataURL(String(locals.tickets[k]._id), { errorCorrectionLevel: 'H' }, function (err, url) {
					locals.tickets[k].qrimage = url;
					check(k);
				});
			}

		} catch (err) {
			console.log(err);
			return next(err);
		}
	});

	view.render('profile/ticket');
};
