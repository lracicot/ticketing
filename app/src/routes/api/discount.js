var keystone = require('keystone');
var Discount = keystone.list('Discount');
var randomstring = require('randomstring');

exports.create = async (req, res) => {
	try {
		const code = randomstring.generate(8);

		if (!req.body.type || !req.body.value || !req.body.event_id) {
			throw new Error('type, value or event_id missing');
		}

		const discount = {
			code: code,
			type: req.body.type,
			value: req.body.value,
			user: req.body.user_id,
			event: req.body.event_id,
		};

		Discount.model.create(discount);
		res.json(discount);
	} catch (err) {
		console.log(err);
		return res.status(418).send(err);
	}
};
