/**
 * This file contains the common middleware used by your routes.
 *
 * Extend or replace these functions as your application requires.
 *
 * This structure is not enforced, and just a starting point. If
 * you have more middleware you may want to group it as separate
 * modules in your project's /lib directory.
 */
var _ = require('lodash');


/**
	Initialises the standard view locals

	The included layout depends on the navLinks array to generate
	the navigation in the header, you may wish to change this array
	or replace it with your own templates / logic.
*/
exports.initLocals = function (req, res, next) {
	res.locals.navLinks = [
		{ label: 'Événements', key: 'events', href: '/events' },
		{ label: 'Poly-Party', key: 'popa', href: '/events' }, // /events/group/popa
		{ label: 'Promo', key: 'promo', href: '/events' }, // /events/group/promo
		{ label: 'Calendrier', key: 'calender', href: '/events' }, // /events/calender
		{ label: 'Contact', key: 'contact', href: '/contact' },
	];
	res.locals.user = req.user;
	// req.protocol
	res.locals.loginRedirect = new Buffer('https://' + req.get('host') + req.originalUrl).toString('base64');
	res.locals.currentUri = req.originalUrl;
	next();
};


/**
	Fetches and clears the flashMessages before a view is rendered
*/
exports.flashMessages = function (req, res, next) {
	var flashMessages = {
		info: req.flash('info'),
		success: req.flash('success'),
		warning: req.flash('warning'),
		error: req.flash('error'),
	};
	res.locals.messages = _.some(flashMessages, function (msgs) { return msgs.length; }) ? flashMessages : false;
	next();
};


/**
	Prevents people from accessing protected pages when they're not signed in
 */
exports.requireUser = function (req, res, next) {
	if (!req.user) {
		req.flash('error', 'Please sign in to access this page.');
		res.redirect('/signin?redirect=' + res.locals.loginRedirect);
	} else {
		next();
	}
};


/**
	Prevents people from accessing organiser pages if they are not organiser
 */
exports.requireOrganiser = function (req, res, next) {
	if (!req.user) {
		req.flash('error', 'Please sign in to access this page.');
		res.redirect('/signin?redirect=' + res.locals.loginRedirect);
	} else {
		if (!req.user.isOrganiser && !req.user.isAdmin) {
			req.flash('error', 'You don\' have the permissions to access this page.');
			res.redirect('/signin?redirect=' + res.locals.loginRedirect);
		} else {
			next();
		}
	}
};


/**
	Prevents people from accessing the api without a valid api token
 */
exports.apiAuth = function (req, res, next) {
	// Error message if the client has no api token
	if (!req.body.apitoken) return res.status(403).send('"apitoken" required.');

	// Hardcoded, for now
	if (req.body.apitoken !== '75663b5ea5c849ef7ceff44eb0cfcd0630b5def7') return res.status(403).send('Invalid API token');

	next();
};
