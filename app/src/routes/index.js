/**
 * This file is where you define your application routes and controllers.
 *
 * Start by including the middleware you want to run for every request;
 * you can attach middleware to the pre('routes') and pre('render') events.
 *
 * For simplicity, the default setup for route controllers is for each to be
 * in its own file, and we import all the files in the /routes/views directory.
 *
 * Each of these files is a route controller, and is responsible for all the
 * processing that needs to happen for the route (e.g. loading data, handling
 * form submissions, rendering the view template, etc).
 *
 * Bind each route pattern your application should respond to in the function
 * that is exported from this module, following the examples below.
 *
 * See the Express application routing documentation for more information:
 * http://expressjs.com/api.html#app.VERB
 */

var keystone = require('keystone');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Import Route Controllers
var routes = {
	views: importRoutes('./views'),
	api: importRoutes('./api'),
};

// Setup Route Bindings
exports = module.exports = function (app) {
	try {
		// Views
		app.get('/', routes.views.index);
		app.get('/contact', routes.views.contact);
		app.post('/contact', routes.views.contact);

		// Event
		app.get('/events', routes.views.event.list);
		app.get('/event/:id', routes.views.event.show);
		app.all('/event/:id/reservation', middleware.requireUser, routes.views.event.reserve);
		app.get('/event/:id/register/:reservation_id', middleware.requireUser, routes.views.event.register);
		app.post('/event/:id/register/:reservation_id', middleware.requireUser, routes.views.event.register);

		// Payment
		app.get('/payment/:reservation_id', middleware.requireUser, routes.views.payment.reservation);
		app.get('/payment/:reservation_id/:attendee_id', middleware.requireUser, routes.views.payment.attendee);
		app.post('/payment/:id', middleware.requireUser, routes.views.payment.validation);
		app.post('/payment/:id/:attendee_id', middleware.requireUser, routes.views.payment.validation);
		app.post('/discount/:attendee_id', middleware.requireUser, routes.views.payment.discount);

		// Profile
		app.get('/profile/tickets', middleware.requireUser, routes.views.profile.tickets);
		app.delete('/profile/tickets/:reservation_id', middleware.requireUser, routes.views.profile.tickets);
		app.get('/profile/ticket/:ticket_id', middleware.requireUser, routes.views.profile.ticket);
		app.get('/memberships', middleware.requireUser, routes.views.profile.memberships);
		app.post('/membership/:membership_id', middleware.requireUser, routes.views.profile.memberships);

		// Event organiser
		app.get('/fix', middleware.requireOrganiser, routes.views.organise.fix);
		app.get('/organise', middleware.requireOrganiser, routes.views.organise.dashboard);
		app.get('/scanner', middleware.requireOrganiser, routes.views.organise.scanner);
		app.post('/scanner', middleware.requireOrganiser, routes.views.organise.scanner);
		app.get('/organise/tickets', middleware.requireOrganiser, routes.views.organise.tickets);
		app.post('/organise/tickets', middleware.requireOrganiser, routes.views.organise.tickets);
		app.get('/organise/groups', middleware.requireOrganiser, routes.views.organise.group);
		app.post('/organise/groups/:group_id', middleware.requireOrganiser, routes.views.organise.group);

		// API endpoints
		app.put('/api/discount', middleware.apiAuth, routes.api.discount.create);

		// Login and signup
		app.all('/join', routes.views.auth.join);
		app.all('/signin', routes.views.auth.signin);
		app.get('/signout', routes.views.auth.signout);
		app.all('/forgotpassword', routes.views.auth.forgotpassword);
		app.all('/resetpassword/:key', keystone.security.csrf.middleware.init, routes.views.auth.resetpassword);
		app.post('/resetpassword/', keystone.security.csrf.middleware.init, routes.views.auth.resetpassword);
	} catch (err) {
		console.error(err);
	}
};
