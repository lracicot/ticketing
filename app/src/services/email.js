var Email = require('keystone-email');

module.exports = function (template, data, to, subject) {
	new Email(template, {
		transport: 'mailgun',
	}).send(data, {
		to: to,
		from: {
			name: process.env.ADMIN_NAME,
			email: process.env.ADMIN_EMAIL,
		},
		subject: subject,
	}, function (err, result) {
		if (err) {
			console.error('🤕 Mailgun test failed with error:\n', err);
		} else {
			console.log('📬 Successfully sent Mailgun test with result:\n', result);
		}
	});
};
