var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Discount Model
 * =============
 */

var Discount = new keystone.List('Discount', {
//	map: { name: 'code' },
	track: true,
});

Discount.add({
	code: { type: Types.Text, initial: true },
	used: { type: Types.Boolean, default: false },
	type: { type: Types.Select, options: 'Amount, Percentage', initial: true },
	value: { type: Types.Number, initial: true },
	user: { type: Types.Relationship, ref: 'User' }, // If the discount is reserved only for a specific user.
	attendee: { type: Types.Relationship, ref: 'Attendee' }, // The attendee that used the discount.
	event: { type: Types.Relationship, ref: 'Event', initial: true },
});

Discount.schema.methods.applyDiscount = function (amount) {
	if (this.type === 'Amount') {
		return Math.max(0, amount - this.value);
	}

	if (this.type === 'Percentage') {
		if (this.value <= 1) {
			return Math.max(0, amount * this.value);
		}

		return Math.max(0, amount - amount * this.value / 100);
	}

	return amount;
};

Discount.defaultSort = '-createdAt';
Discount.defaultColumns = 'event, type, value, used, code, _id';
Discount.register();
