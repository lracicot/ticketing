var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Group Model
 * =============
 */

var CustomField = new keystone.List('CustomField', {
	track: true,
});

CustomField.add({
	name: { type: Types.Text, required: true, default: '', initial: true },
	type: { type: Types.Select, options: ['Text', 'Select', 'Url', 'Textarea', 'Date', 'Boolean'], initial: true },
	options: { type: Types.Textarea, dependsOn: { type: 'Select' } },
	label: { type: Types.Text, initial: true },
	group: { type: Types.Relationship, ref: 'Group' },
});

CustomField.schema.virtual('getFieldName').get(function (value) {
	return this.name + this.group;
});

CustomField.schema.virtual('render').get(function (value) {
	const id = this.name + this.group;
	let str = '<label>' + this.label + '</label>';

	if (this.type === 'Text') {
		str += '<input type="text" name="' + id + '" id="' + id + '" class="form-control">';
	}

	if (this.type === 'Date') {
		str += '<input type="date" name="' + id + '" id="' + id + '" class="form-control">';
	}

	if (this.type === 'Url') {
		str += '<input type="url" name="' + id + '" id="' + id + '" class="form-control">';
	}

	if (this.type === 'Select') {
		str += '<select name="' + id + '" id="' + id + '" class="form-control">';

		for (const option of this.options.split(',')) {
			str += '<option value="' + option.trim() + '">' + option.trim() + '</option>';
		}

		str += '</select>';
	}

	if (this.type === 'Textarea') {
		str += '<textarea name="' + id + '" id="' + id + '" class="form-control"></textarea>';
	}

	if (this.type === 'Boolean') {
		str += '<input type="hidden" name="' + id + '" value="false">';
		str += '<input type="checkbox" name="' + id + '" id="' + id + '" value="true">';
	}

	return str;
});

CustomField.defaultSort = '-createdAt';
CustomField.defaultColumns = 'name, type';
CustomField.register();
