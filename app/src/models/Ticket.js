var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Ticket Model
 * =============
 */

var Ticket = new keystone.List('Ticket', {
	track: true,
});

Ticket.add({
	used: { type: Types.Boolean, default: false },
	seat: { type: Types.Number, default: false },
	email: { type: Types.Text, default: false },
	name: { type: Types.Text, default: false },
	event: { type: Types.Relationship, ref: 'Event' },
	user: { type: Types.Relationship, ref: 'User' },
	reservation: { type: Types.Relationship, ref: 'Reservation' },
	ticket_type: { type: Types.Relationship, ref: 'TicketType' },
});

Ticket.defaultSort = '-createdAt';
Ticket.register();
