var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Event Model
 * =============
 */

var Event = new keystone.List('Event', {
	map: { name: 'title' },
	track: true,
});

var localStorage = new keystone.Storage({
	adapter: keystone.Storage.Adapters.FS,
	fs: {
		path: keystone.expandPath('./public/uploads'),
		publicPath: '/uploads',
	},
});

Event.add({
	title: { type: Types.Text, required: true, initial: true },
	start: { type: Types.Datetime, required: true, initial: true },
	end: { type: Types.Datetime, required: true, initial: true },
	registration_start: { type: Types.Datetime, default: Date.now },
	registration_end: { type: Types.Datetime },
	description: { type: Types.Markdown, required: true, default: '' },
	location: { type: Types.Text, required: false, default: '' },
	number_of_tickets: { type: Types.Number, default: 0 },
	max_buy_tickets: { type: Types.Number, default: 1 },
	min_buy_tickets: { type: Types.Number, default: 0 },
	image: {
		type: Types.File,
		storage: localStorage,
	},
	group: { type: Types.Relationship, ref: 'Group' },
	organiser: { type: Types.Relationship, ref: 'User' },
});

Event.relationship({ path: 'ticket_types', ref: 'TicketType', refPath: 'event', createInline: true });
Event.relationship({ path: 'tickets', ref: 'Ticket', refPath: 'event' });
Event.relationship({ path: 'reservations', ref: 'Reservation', refPath: 'event' });
Event.relationship({ path: 'discounts', ref: 'Discount', refPath: 'event' });

Event.schema.methods.getTicketTypes = async function () {
	return await keystone.list('TicketType').model.find({ event: this._id }).exec();
};

Event.schema.methods.areRegistrationsOpen = function () {
	const now = new Date();

	if (this.registration_start && (now.getTime() < this.registration_start.getTime())) {
		return false;
	}

	if (this.registration_end && (now.getTime() >= this.registration_end.getTime())) {
		return false;
	}

	return true;
};

Event.schema.methods.getAvailableTickets = async function (callback) {
	const results = await keystone.list('Reservation').model
		.aggregate([{
			$match: {
				event: this._id,
				unpaidTimeout: { $gt: new Date() },
			},
		}, {
			$group: {
				_id: null,
				total: {
					$sum: '$number_of_tickets',
				},
			},
		}]).exec();

	const countReservations = (results.length > 0) ? results[0].total : 0;
	const countTickets = await keystone.list('Ticket').model.find({ event: this._id }).count().exec();

	return this.number_of_tickets - countReservations - countTickets;
};

Event.defaultSort = '-createdAt';
Event.defaultColumns = 'title, start, location, ticket_price';
Event.register();
