var keystone = require('keystone');
var Types = keystone.Field.Types;
var moment = require('moment');

/**
 * Reservation Model
 * =============
 */

var Reservation = new keystone.List('Reservation', {
	track: true,
});

Reservation.add({
	unpaidTimeout: { type: Types.Datetime },
	number_of_tickets: { type: Types.Number },
	individual_payment: { type: Types.Boolean },
	event: { type: Types.Relationship, ref: 'Event' },
});

Reservation.schema.pre('remove', function (next) {
	keystone.list('Attendee').model.remove({ reservation: this._id }).exec();
	next();
});

Reservation.relationship({ path: 'tickets', ref: 'Ticket', refPath: 'reservation' });
Reservation.relationship({ path: 'attendees', ref: 'Attendee', refPath: 'reservation' });

Reservation.schema.virtual('remainingTime').get(function () {
	return moment(moment(this.unpaidTimeout) - moment());
});

Reservation.schema.virtual('isExpired').get(function () {
	return moment(this.unpaidTimeout).diff(moment()) <= 0;
});

Reservation.schema.methods.getAttendees = function () {
	return keystone.list('Attendee').model.find({ reservation: this._id }).populate('ticket_type').exec();
};

Reservation.schema.methods.getRemainingPrice = async function () {
	let total = 0;
	for (const attendee of await this.getAttendees()) {
		if (!attendee.paid) {
			total += await attendee.getPrice();
		}
	}

	return total;
};

Reservation.schema.methods.getTickets = async function () {
	return keystone.list('Ticket').model.find({
		reservation: this._id,
	});
};

Reservation.schema.methods.isPaid = async function () {
	for (const attendee of await this.getAttendees()) {
		if (!attendee.paid) return false;
	}

	return true;
};

Reservation.schema.methods.setTimeoutTo = async function (value, unit) {
	this.unpaidTimeout = moment().add(value, unit);
};

Reservation.schema.methods.isStillValid = async function () {
	const event = await keystone.list('Event').model.findById(this.event);
	return !this.isExpired || (await event.getAvailableTickets()) > 0;
};

Reservation.schema.methods.getAttendeeByUser = async function (userId) {
	return keystone.list('Attendee').model.findOne({
		reservation: this._id,
		user: userId,
	});
};

Reservation.schema.methods.isPaidFor = async function (userId) {
	return (await this.getAttendeeByUser(userId)).paid;
};

Reservation.schema.methods.getTotalPrice = async function () {
	let total_price = 0;

	for (const attendee of await this.getAttendees()) {
		total_price += await attendee.getPrice();
	}

	return total_price;
};

Reservation.defaultSort = '-createdAt';
Reservation.defaultColumns = 'title, start, location, ticket_price';
Reservation.register();
