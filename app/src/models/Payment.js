var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Payment Model
 * =============
 */

var Payment = new keystone.List('Payment', {
	track: true,
});

Payment.add({
	data: { type: Types.Text },
	user: { type: Types.Relationship, ref: 'User' },
	reservation: { type: Types.Relationship, ref: 'Reservation' },
});

Payment.defaultSort = '-createdAt';
Payment.register();
