var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Group Model
 * =============
 */

var Group = new keystone.List('Group');

Group.add({
	name: { type: Types.Text, required: true },
	createdAt: { type: Date, default: Date.now },
	owner: { type: Types.Relationship, ref: 'User' },
});

Group.relationship({ path: 'membership', ref: 'Membership', refPath: 'group' });
Group.relationship({ path: 'events', ref: 'Event', refPath: 'groups' });
Group.relationship({ path: 'customFields', ref: 'CustomField', refPath: 'group', createInline: true });

Group.schema.methods.getMembers = function () {
	return keystone.list('Membership').model.find({ group: this._id }).populate('user').exec();
};

Group.defaultSort = '-createdAt';
Group.defaultColumns = 'name, createdAt';
Group.register();
