var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Membership Model
 * =============
 */

var Membership = new keystone.List('Membership', {
	track: true,
});

Membership.add({
	user: { type: Types.Relationship, ref: 'User' },
	group: { type: Types.Relationship, ref: 'Group' },
	email: { type: Types.Email },
	expiration: { type: Types.Date },
});

keystone.list('Group').model.aggregate([
	{ $lookup: {
		from: 'customfields',
		localField: '_id',
		foreignField: 'group',
		as: 'fields',
	} },
]).exec().then(function (groups) {
	for (const group of groups) {
		for (const field of group.fields) {
			let newField = {};
			newField[field.name + group._id] = {
				type: Types[field.type],
				label: group.name + ': ' + field.label,
				dependsOn: { group: { $regex: '.*' + group._id + '.*/i' } },
			};

			if (field.type === 'Select') {
				newField[field.name + group._id].options = field.options.split(',');
			}

			Membership.add(newField);
		}
	}
});

Membership.defaultSort = '-createdAt';
Membership.defaultColumns = 'email, group, createdAt';
Membership.register();
