var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * TicketType Model
 * =============
 */

var TicketType = new keystone.List('TicketType', {
	map: { name: 'description' },
	track: true,
});

TicketType.add({
	description: { type: Types.Text, initial: true },
	price: { type: Types.Money, initial: true },
	max_buy: { type: Types.Number, initial: true },
	event: { type: Types.Relationship, ref: 'Event', initial: true },
});

TicketType.defaultSort = '-createdAt';
TicketType.defaultColumns = 'description, price';
TicketType.register();
