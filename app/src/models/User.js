var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * User Model
 * ==========
 */
var User = new keystone.List('User');

User.add({
	name: { type: Types.Name, required: true, index: true },
  company: { type: String },
	email: { type: Types.Email, initial: true, required: true, unique: true, index: true },
	password: { type: Types.Password, initial: true, required: true },
	resetPasswordKey: { type: String, hidden: true },
}, 'Permissions', {
	isAdmin: { type: Boolean, label: 'Can access Keystone', index: true },
	isOrganiser: { type: Boolean, label: 'Can organise events' },
});

User.relationship({ path: 'membership', ref: 'Membership', refPath: 'group' });

// Provide access to Keystone
User.schema.virtual('canAccessKeystone').get(function () {
	return this.isAdmin;
});
// Provide access to Keystone
User.schema.virtual('canOrganiseEvents').get(function () {
	return this.isAdmin || this.isOrganiser;
});
User.schema.virtual('full_name').get(function () {
	return this.name.first + ' ' + this.name.last;
});

User.schema.methods.getEventReservation = function (eventId) {
	return keystone.list('Reservation').model.aggregate([
		{ $lookup: {
			from: 'attendees',
			localField: '_id',
			foreignField: 'reservation',
			as: 'myself',
		} },
		{ $match: {
			event: eventId,
		} },
		{ $project: {
			myself: {
				$filter: {
					input: '$myself',
					as: 'me',
					cond: { $eq: ['$$me.user', this._id] },
				},
			},
			number_of_tickets: 1,
			individual_payment: 1,
		} },
		{ $match: {
			myself: { $ne: [] },
		} },
	]).exec();
};

User.schema.methods.getReservations = function () {
	return keystone.list('Reservation').model.aggregate([
		{ $lookup: {
			from: 'events',
			localField: 'event',
			foreignField: '_id',
			as: 'event',
		} },
		{ $lookup: {
			from: 'attendees',
			localField: '_id',
			foreignField: 'reservation',
			as: 'guests',
		} },
		{ $lookup: {
			from: 'attendees',
			localField: '_id',
			foreignField: 'reservation',
			as: 'myself',
		} },
		{ $project: {
			myself: {
				$filter: {
					input: '$myself',
					as: 'me',
					cond: { $eq: ['$$me.user', this._id] },
				},
			},
			guests: {
				$filter: {
					input: '$guests',
					as: 'guest',
					cond: { $ne: ['$$guest.user', this._id] },
				},
			},
			event: { $arrayElemAt: ['$event', 0] },
			number_of_tickets: 1,
			individual_payment: 1,
		} },
		{ $match: {
			myself: { $ne: [] },
		} },
	]).exec();
};

User.schema.methods.isInGroup = async function (group_id) {
	return (await keystone.list('Membership').model.find({ user: this._id, group: group_id }).exec()).length > 0;
};

User.schema.methods.getMemberships = function () {
	return keystone.list('Membership').model.find({ user: this._id }).populate('group').populate('customfields').lean().exec();
};

User.schema.methods.getTickets = function () {
	return keystone.list('Ticket').model.find({ user: this._id });
};

User.relationship({ path: 'tickets', ref: 'Ticket', refPath: 'user' });
User.relationship({ path: 'attendees', ref: 'Attendee', refPath: 'user' });

/**
 * Registration
 */
User.defaultColumns = 'name, email, isAdmin';
User.register();
