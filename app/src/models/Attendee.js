var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Attendee Model
 * =============
 */

var Attendee = new keystone.List('Attendee', {
	map: { name: 'email' },
	track: true,
});

Attendee.add({
	paid: { type: Types.Boolean, default: false },
	seat: { type: Types.Number, default: false },
	email: { type: Types.Email, default: false },
	name: { type: Types.Text, default: false },
	user: { type: Types.Relationship, ref: 'User' },
	reservation: { type: Types.Relationship, ref: 'Reservation' },
	ticket_type: { type: Types.Relationship, ref: 'TicketType' },
});

Attendee.relationship({ path: 'discount', ref: 'Discount', refPath: 'attendee' });

Attendee.schema.methods.getDiscount = async function () {
	return await keystone.list('Discount').model.findOne({ attendee: this._id }).exec();
};

Attendee.schema.methods.getPrice = async function (amount) {
	const ticket_type = await keystone.list('TicketType').model.findById(this.ticket_type);
	const discount = await keystone.list('Discount').model.findOne({ attendee: this._id }).exec();

	if (discount) {
		return discount.applyDiscount(ticket_type.price);
	}

	return ticket_type.price;
};

Attendee.defaultSort = '-createdAt';
Attendee.defaultColumns = 'email';
Attendee.register();
